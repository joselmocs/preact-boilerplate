import { h, Component } from "preact";
import Header from "../common/header";
import TopMenu from "../common/topmenu";


class Buttons extends Component {
    render = () => (
        <div class="page-container">
            <Header />
            <TopMenu />
            <div class="content">
                <h1 class="page-header">
                    Buttons <small>examples...</small>
                </h1>

                <div class="">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">Buttons</div>
                        </div>

                        <div class="panel-body clf">
                            <div class="mb-5">
                                <button type="button" class="btn mr-5">Default</button>
                                <button type="button" class="btn btn-primary disabled btn-loading mr-5">Primary</button>
                                <button type="button" class="btn btn-warning btn-loading mr-5">Warning</button>
                                <button type="button" class="btn btn-info btn-sm mr-5">Info</button>
                                <button type="button" class="btn btn-danger mr-5">Danger</button>
                            </div>
                            <div class="mb-5">
                                <button type="button" class="btn btn-outline disabled mr-5">Default</button>
                                <button type="button" class="btn btn-outline btn-primary mr-5">Primary</button>
                                <button type="button" class="btn btn-outline btn-warning mr-5">Warning</button>
                                <button type="button" class="btn btn-outline btn-info btn-loading mr-5">Info</button>
                                <button type="button" class="btn btn-outline btn-danger btn-xs mr-5">Danger</button>
                            </div>

                            <div class="mb-5 clf">
                                <div class="btn-group">
                                    <button type="button" class="btn">Default</button>
                                    <button type="button" class="btn btn-primary">Primary</button>
                                </div>
                            </div>

                            <div class="mb-5 btn-toolbar clf">
                                <div class="btn-group">
                                    <button type="button" class="btn">1</button>
                                    <button type="button" class="btn active">2</button>
                                    <button type="button" class="btn">3</button>
                                    <button type="button" class="btn btn-primary">4</button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn">5</button>
                                    <button type="button" class="btn">6</button>
                                    <button type="button" class="btn">7</button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn">8</button>
                                </div>
                            </div>

                            <div class="mb-5 clf">
                                <button type="button" class="btn mr-5">
                                    <span class="btn-label left">1,307</span>Default
                                </button>
                                <button type="button" class="btn btn-sm btn-primary mr-5">
                                    <span class="btn-label right">1,307</span>Primary
                                </button>
                                <button type="button" class="btn btn-xs btn-info">
                                    <span class="btn-label right">1,307</span>Info
                                </button>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default Buttons;
