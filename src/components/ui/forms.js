import { h, Component } from "preact";
import Header from "../common/header";
import TopMenu from "../common/topmenu";


class Forms extends Component {
    render = () => (
        <div class="page-container">
            <Header />
            <TopMenu />
            <div class="content">
                <h1 class="page-header">
                    Forms <small>examples...</small>
                </h1>

                <div class="clf">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">form-inline</div>
                        </div>

                        <div class="panel-body">
                            <form class="form-inline" action="javascript:">
                                <div class="form-group mr-10">
                                    <input type="email" class="form-control" placeholder="Enter email" />
                                </div>
                                <div class="form-group mr-10">
                                    <input type="password" class="form-control" placeholder="Password" />
                                </div>
                                <div class="checkbox mr-10">
                                    <label>
                                        <input type="checkbox" /> Remember me
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary mr-5">Sign in</button>
                                <button type="submit" class="btn">Register</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row clf">

                    <div class="col-xs-12 col-md-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">form</div>
                            </div>
                            <div class="panel-body">
                                <legend>Legend</legend>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" />
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" />
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" /> Check me out
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary mr-5">Login</button>
                                <button type="submit" class="btn ">Cancel</button>
                            </div>
                            <div class="panel-footer">
                                <p class="mb-0">legend panel-footer</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">form-horizontal</div>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" action="javascript:">
                                    <fieldset>
                                        <legend>
                                            Legend
                                            <button type="submit" class="btn btn-info btn-outline btn-sm pull-right">Edit</button>
                                        </legend>
                                        <div class="form-group clf has-error">
                                            <label class="col-md-4 control-label">Email address</label>
                                            <div class="col-md-8">
                                                <input type="email" class="form-control" placeholder="Enter email" />
                                                <div class="help-block">Please choose a unique and valid username.</div>
                                            </div>
                                        </div>
                                        <div class="form-group clf">
                                            <label class="col-md-4 control-label">Password</label>
                                            <div class="col-md-8">
                                                <input type="password" class="form-control" placeholder="Password" />
                                            </div>
                                        </div>
                                        <div class="form-group clf">
                                            <div class="col-md-8 col-md-offset-4">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" /> Check me out
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group clf">
                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary mr-5">Login</button>
                                                <button type="submit" class="btn">Cancel</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="panel-footer">
                                <p class="mb-0">legend panel-footer</p>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row clf">

                    <div class="col-xs-12 col-md-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">types</div>
                            </div>
                            <div class="panel-body clf">
                                <form class="form-horizontal">

                                    <div class="form-group clf">
                                        <label class="col-md-3 control-label">input</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" placeholder="Default input" />
                                        </div>
                                    </div>

                                    <div class="form-group clf">
                                        <label class="col-md-3 control-label">input.input-sm</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control input-sm" placeholder="Default input" />
                                        </div>
                                    </div>

                                    <div class="form-group clf">
                                        <label class="col-md-3 control-label">input[disabled]</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" placeholder="Disabled input" disabled />
                                        </div>
                                    </div>

                                    <div class="form-group clf">
                                        <label class="col-md-3 control-label">select</label>
                                        <div class="col-md-9">
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group clf">
                                        <label class="col-md-3 control-label">select.input-sm</label>
                                        <div class="col-md-9">
                                            <select class="form-control input-sm">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group clf">
                                        <label class="col-md-3 control-label">textarea</label>
                                        <div class="col-md-9">
                                            <textarea class="form-control" placeholder="Textarea" rows="5" />
                                        </div>
                                    </div>

                                    <div class="form-group clf">
                                        <label class="col-md-3 control-label">checkbox</label>
                                        <div class="col-md-9">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="" />
                                                    Checkbox Label 1
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="" />
                                                    Checkbox Label 2
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clf">
                                        <label class="col-md-3 control-label">checkbox-inline</label>
                                        <div class="col-md-9">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" value="" />
                                                Checkbox Label 1
                                            </label>
                                            <label class="checkbox-inline">
                                                <input type="checkbox" value="" />
                                                Checkbox Label 2
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group clf">
                                        <label class="col-md-3 control-label">rario</label>
                                        <div class="col-md-9">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="optionsRadios" value="option1" checked />
                                                    Radio option 1
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="optionsRadios" value="option2" />
                                                    Radio option 2
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group clf">
                                        <label class="col-md-3 control-label">radio-inline</label>
                                        <div class="col-md-9">
                                            <label class="radio-inline">
                                                <input type="radio" name="optionsRadios-inline" value="option1" checked />
                                                Radio option 1
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="optionsRadios-inline" value="option2" />
                                                Radio option 2
                                            </label>
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default Forms;
