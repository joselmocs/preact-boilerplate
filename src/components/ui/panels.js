import { h, Component } from "preact";
import Header from "../common/header";
import TopMenu from "../common/topmenu";


class Panels extends Component {
    render = () => (
        <div class="page-container">
            <Header />
            <TopMenu />
            <div class="content">
                <h1 class="page-header">
                    Panels <small>examples...</small>
                </h1>

                <div class="row clf">

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">panel</div>
                            </div>

                            <div class="panel-body">
                                content
                            </div>

                            <div class="panel-footer">
                                <p class="mb-0">legend panel-footer</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">panel panel-default</div>
                            </div>

                            <div class="panel-body">
                                content
                            </div>

                            <div class="panel-footer">
                                <button type="button" class="btn">cancel</button>

                                <div class="btn-toolbar pull-right">
                                    <button type="button" class="btn btn-info btn-outline mr-5">next</button>
                                    <button type="button" class="btn btn-primary btn-outline">done</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="panel panel-loading">
                            <div class="panel-heading">
                                <div class="panel-title">panel panel-loading</div>
                            </div>

                            <div class="panel-body">
                                <div class="panel-loader">
                                    <span class="spinner-small" />
                                </div>
                                loading content
                            </div>

                            <div class="panel-footer">
                                <div class="btn-toolbar clf">
                                    <div class="btn-group btn-group-sm">
                                        <button type="button" class="btn">1</button>
                                        <button type="button" class="btn active">2</button>
                                        <button type="button" class="btn">3</button>
                                        <button type="button" class="btn btn-primary">4</button>
                                    </div>
                                    <div class="btn-group btn-group-sm">
                                        <button type="button" class="btn disabled">5</button>
                                        <button type="button" class="btn">6</button>
                                        <button type="button" class="btn">7</button>
                                    </div>
                                    <div class="btn-group btn-group-sm">
                                        <button type="button" class="btn">8</button>
                                    </div>

                                    <div class="pull-right clf">
                                        <button type="button" class="btn btn-sm btn-outline">close</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default Panels;
