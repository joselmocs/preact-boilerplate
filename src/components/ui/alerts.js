import { h, Component } from "preact";
import Header from "../common/header";
import TopMenu from "../common/topmenu";


class Alerts extends Component {
    render = () => (
        <div class="page-container">
            <Header />
            <TopMenu />
            <div class="content">
                <h1 class="page-header">
                    Alerts <small>examples...</small>
                </h1>

                <div class="row clf">
                    <div class="col-xs-12 col-sm-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">Alerts</div>
                            </div>
                            <div class="alert alert-success">
                                <h4 class="alert-heading">Well done!</h4>
                                <p class="mb-0">alert alert-success</p>
                            </div>

                            <div class="panel-body clf">
                                <div class="alert"><p class="mb-0">alert</p></div>
                                <div class="alert alert-success"><p class="mb-0">alert alert-success</p></div>
                                <div class="alert alert-info"><p class="mb-0">alert alert-info</p></div>
                                <div class="alert alert-warning"><p class="mb-0">alert alert-warning</p></div>
                                <div class="alert alert-danger"><p class="mb-0">alert alert-danger</p></div>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">Notes</div>
                            </div>

                            <div class="panel-body clf">
                                <div class="note">
                                    <h4>Note!</h4>
                                    <p class="mb-0">note</p>
                                </div>
                                <div class="note note-success">
                                    <h4>Note!</h4>
                                    <p class="mb-0">note note-success</p>
                                </div>
                                <div class="note note-info">
                                    <h4>Note!</h4>
                                    <p class="mb-0">note note-info</p>
                                </div>
                                <div class="note note-warning">
                                    <h4>Note!</h4>
                                    <p class="mb-0">note note-warning</p>
                                </div>
                                <div class="note note-danger">
                                    <h4>Note!</h4>
                                    <p class="mb-0">note note-danger</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default Alerts;
