import { h, Component } from "preact";
import Header from "../common/header";
import TopMenu from "../common/topmenu";


class Home extends Component {
    render = () => (
        <div class="page-container">
            <Header />
            <TopMenu />
            <div class="content">
                <h1>Home</h1>
            </div>
        </div>
    )
}

export default Home;
