import { route } from "preact-router";
import { set as setCookie, erase as eraseCookie } from "browser-cookies";


export const actions = (store) => ({
    login: (state, username) => {
        store.setState({
            auth: {
                username,
                verified: true
            }
        });

        setCookie("sessionid", "ajshdh21348yhsf");
        setCookie("auth", JSON.stringify({
            username
        }));

        route("/", true);
    },
    logout: (/* state */) => {
        store.setState({
            auth: {
                verified: true
            }
        });

        eraseCookie("auth");
        eraseCookie("sessionid");

        route("/", true);
    }
});

