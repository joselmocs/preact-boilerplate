import { h, Component } from "preact";
import { connect } from "unistore/preact";
import { route } from 'preact-router';

import { actions as authActions } from "./actions";
import Header from "../common/header";
import TopMenu from "../common/topmenu";


class Login extends Component {
    state = {
        loading: false,
        username: {
            value: "",
            error: null
        },
        password: {
            value: "",
            error: null
        }
    }

    componentWillMount() {
        if (this.props.auth.username) {
            setTimeout(() => route("/profile", true));
        }
    }

    login = (event) => {
        event.preventDefault();

        this.setState({
            ...this.state,
            loading: true
        });

        let usernameError = null,
            passwordError = null;

        if (this.state.username.value.trim() === "") {
            usernameError = "Please type a unique and valid username.";
        }

        if (this.state.password.value.trim() === "") {
            passwordError = "Please type a valid password.";
        }

        this.setState({
            ...this.state,
            username: {
                ...this.state.username,
                error: usernameError
            },
            password: {
                ...this.state.password,
                error: passwordError
            }
        });

        if (!usernameError && !passwordError) {
            setTimeout(() => {
                this.props.login(this.state.username.value);
                this.setState({
                    ...this.state,
                    loading: false
                });
            }, 3000);
        }
        else {
            this.setState({
                ...this.state,
                loading: false
            });
        }
    }

    render = () => {
        return (
            <div class="page-container">
                <Header />
                <TopMenu />
                <div class="content">
                    <h1 class="page-header">
                        Page with Top Menu <small>header small text goes here...</small>
                    </h1>

                    <div class="row clf">

                        <div class="col-xs-12 col-sm-6">
                            <div class="panel">
                                <div class="panel-heading">
                                    <div class="panel-title">panel</div>
                                </div>

                                <div class="panel-body">
                                    <form onSubmit={this.login} action="javascript:">
                                        <legend>Legend</legend>
                                        <div class={this.state.username.error ? 'form-group has-error' : 'form-group'}>
                                            <label for="inputUsername">Username</label>
                                            <input
                                                type="text"
                                                class="form-control"
                                                id="inputUsername"
                                                placeholder="Username"
                                                disabled={this.state.loading}
                                                value={this.state.username.value}
                                                onInput={this.linkState('username.value')}
                                            />
                                            { this.state.username.error && (<span class="help-block">{this.state.username.error}</span>) }
                                        </div>

                                        <div class={this.state.password.error ? 'form-group has-error' : 'form-group'}>
                                            <label for="inputPassword">Password</label>
                                            <input
                                                type="password"
                                                class="form-control"
                                                id="inputPassword"
                                                placeholder="Password"
                                                disabled={this.state.loading}
                                                value={this.state.password.value}
                                                onInput={this.linkState('password.value')}
                                            />
                                            { this.state.password.error && (<span class="help-block">{this.state.password.error}</span>) }
                                        </div>

                                        <div class="checkbox">
                                            <label>
                                                <input
                                                    type="checkbox"
                                                    disabled={this.state.loading}
                                                /> Remember username
                                            </label>
                                        </div>

                                        <button
                                            type="submit"
                                            class={this.state.loading ? 'btn btn-primary btn-loading mr-5' : 'btn btn-primary mr-5'}
                                            disabled={this.state.loading}
                                        >
                                            Login
                                        </button>
                                    </form>
                                </div>

                                <div class="panel-footer">
                                    <p class="mb-0">legend panel-footer</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6">
                            <div class={this.state.loading ? 'panel panel-loading' : 'panel'}>
                                <div class="panel-heading">
                                    <div class="panel-title">panel loading</div>
                                </div>

                                <div class="panel-body">
                                    {this.state.loading && (
                                        <div class="panel-loader">
                                            <span class="spinner-small" />
                                        </div>
                                    )}
                                    <form class="form-horizontal" onSubmit={this.login} action="javascript:">
                                        <legend>Legend</legend>
                                        <div class={this.state.username.error ? 'form-group clf has-error' : 'form-group clf'}>
                                            <label class="col-md-4 control-label">Username</label>
                                            <div class="col-md-8">
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    placeholder="Username"
                                                    value={this.state.username.value}
                                                    onInput={this.linkState('username.value')}
                                                />
                                                { this.state.username.error && (<span class="help-block">{this.state.username.error}</span>) }
                                            </div>
                                        </div>

                                        <div class={this.state.password.error ? 'form-group clf has-error' : 'form-group clf'}>
                                            <label class="col-md-4 control-label">Password</label>
                                            <div class="col-md-8">
                                                <input
                                                    type="password"
                                                    class="form-control"
                                                    placeholder="Password"
                                                    value={this.state.password.value}
                                                    onInput={this.linkState('password.value')}
                                                />
                                                { this.state.password.error && (<span class="help-block">{this.state.password.error}</span>) }
                                            </div>
                                        </div>

                                        <div class="form-group clf">
                                            <div class="col-md-8 col-md-offset-4">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" /> Remember username
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group mb-0 clf">
                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">Login</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="panel-footer">
                                    <p class="mb-0">legend panel-footer</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        auth: state.auth
    }),
    authActions
)(Login);
