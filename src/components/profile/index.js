import { h, Component } from "preact";
import { connect } from "unistore/preact";

import Header from "../common/header";
import TopMenu from "../common/topmenu";


class Profile extends Component {
    render = () => (
        <div class="page-container">
            <Header />
            <TopMenu />
            <div class="content">
                { this.props.user && (
                    <h1>Profile of {this.props.user}</h1>
                )}
                { !this.props.user && this.props.username && (
                    <h1>My profile</h1>
                )}
            </div>
        </div>
    )
}

export default connect(
    (state) => ({
        username: state.auth.username
    })
)(Profile);
