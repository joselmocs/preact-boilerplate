import { h, Component } from "preact";
import { connect } from "unistore/preact";

import { actions as authActions } from "../auth/actions";


class Header extends Component {
    state = {
        searchText: ""
    }

    render = () => {
        return (
            <div class="navbar clf">
                <div class="navbar-header">
                    <a href="/" class="navbar-brand">{this.props.page.title}</a>
                </div>
                <ul class="nav navbar-nav navbar-right clf">
                    <li>
                        <form class="navbar-form">
                            <div class="form-group">
                                <input
                                    type="text"
                                    class="form-control"
                                    placeholder="#"
                                    value={this.state.searchText}
                                    onInput={this.linkState('searchText')}
                                />
                            </div>
                        </form>
                    </li>
                    { this.props.auth.username && (
                        <li class="dropdown">
                            <a href="javascript:" data-toggle="dropdown">
                                <span>{this.props.auth.username}</span> <b class="caret" />
                            </a>
                            <ul class="dropdown-menu">
                                <li class="arrow" />
                                <li><a href="/profile">My Profile</a></li>
                                <li><a href="javascript:">Setting</a></li>
                                <li class="divider" />
                                <li><a href="javascript:" onClick={this.props.logout}>Log Out</a></li>
                            </ul>
                        </li>
                    )}
                </ul>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        page: state.page,
        auth: state.auth
    }),
    authActions
)(Header);
