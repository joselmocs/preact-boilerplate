import { h, Component } from "preact";
import { connect } from "unistore/preact";
import { getCurrentUrl } from 'preact-router';

import { actions as authActions } from "../auth/actions";


class TopMenu extends Component {
    render = () => {
        const currentUrl = getCurrentUrl();

        return (
            <div id="top-menu">
                <ul class="nav clf">
                    <li class={currentUrl === "/" ? "active" : ""}>
                        <a href="/">
                            <span>Dashboard</span>
                        </a>
                    </li>

                    <li class={currentUrl === "/profile/john" ? "active" : ""}>
                        <a href="/profile/john">
                            <span>John</span>
                        </a>
                    </li>

                    { !this.props.auth.username && (
                        <li class={currentUrl === "/login" ? "active" : ""}>
                            <a href="/login">Login</a>
                        </li>
                    )}

                    <li class={currentUrl.indexOf("/ui") !== -1 ? "has-sub active" : "has-sub"}>
                        <a href="javascript:">
                            <b class="caret pull-right" />
                            <span>ui</span>
                        </a>
                        <ul class="sub-menu">
                            <li class={currentUrl === "/ui/panels" ? "active" : ""}>
                                <a href="/ui/panels">panels</a>
                            </li>
                            <li class={currentUrl === "/ui/buttons" ? "active" : ""}>
                                <a href="/ui/buttons">buttons</a>
                            </li>
                            <li class={currentUrl === "/ui/alerts" ? "active" : ""}>
                                <a href="/ui/alerts">alerts</a>
                            </li>
                            <li class={currentUrl === "/ui/forms" ? "active" : ""}>
                                <a href="/ui/forms">forms</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        auth: state.auth
    }),
    authActions
)(TopMenu);
