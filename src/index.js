import 'linkstate/polyfill';
import { h, render } from 'preact';
import createStore from 'unistore';
import { Provider } from 'unistore/preact';

import initialStore from './app/store';
import App from './app';


let store;
if (process.env.NODE_ENV === 'production') {
    store = createStore(initialStore);
}
else {
    require('preact/debug');
    store = require('unistore/devtools')(createStore(initialStore));
    // store.subscribe(console.log);
}

const root = document.getElementById("root");
function init() {
    render(<Provider store={store}><App/></Provider>, root, root.lastChild);
}

if (module.hot) {
    module.hot.accept('./app', () => requestAnimationFrame(init));
}

init();
