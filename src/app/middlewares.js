import { route } from "preact-router";


const authMiddleware = (store, state, routerProps) => {
    if (!state.auth.username && routerProps.requireAuth) {
        route(routerProps.requireAuth || "/", true);
        return true;
    }
};

const middlewares = [
    authMiddleware
];

export default middlewares;
