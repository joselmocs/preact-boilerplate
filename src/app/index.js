import { h, Component } from "preact";
import { Router, Route } from "preact-router";
import { connect } from "unistore/preact";

import { actions as appActions } from "./actions";
import Home from "../components/home";
import Profile from "../components/profile";
import Login from "../components/auth/login";
import { PageNotFound } from "../components/common/pages";
import Panels from "../components/ui/panels";
import Buttons from "../components/ui/buttons";
import Alerts from "../components/ui/alerts";
import Forms from "../components/ui/forms";

import "styles/app.scss";


class App extends Component {
    componentDidMount = () => this.props.fetchSession();

    render = () => {
        if (!this.props.verified) {
            return <div>checking user session</div>;
        }

        return (
            <Router onChange={this.props.handleRouterOnChange}>
                <Route path="/" component={Home} />
                <Route path="/login" component={Login} />
                <Route path="/profile" requireAuth="/login" component={Profile} />
                <Route path="/profile/:user" component={Profile} />
                <Route path="/ui/panels" component={Panels} />
                <Route path="/ui/buttons" component={Buttons} />
                <Route path="/ui/alerts" component={Alerts} />
                <Route path="/ui/forms" component={Forms} />
                <Route default component={PageNotFound} />
            </Router>
        );
    }
}

export default connect(
    (state) => ({
        verified: state.auth.verified
    }),
    appActions
)(App);
