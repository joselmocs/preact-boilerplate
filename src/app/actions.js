import { get as getCookie } from "browser-cookies";

import middlewares from "./middlewares";


export const actions = (store) => ({
    fetchSession: (state) => {
        let session = getCookie("sessionid");
        if (!session) {
            return {
                ...state,
                auth: {
                    ...state.auth,
                    verified: true
                }
            };
        }

        setTimeout(() => {
            let auth = getCookie("auth");
            auth = (auth) ? JSON.parse(auth) : {};
            auth["verified"] = true;
            store.setState({ ...state, auth });
        }, 100);
    },
    handleRouterOnChange: (state, router) => {
        if (!router.current) {
            return;
        }

        middlewares.some((middleware) => {
            if (middleware(store, state, router.current.attributes)) {
                return;
            }
        });
    }
});
