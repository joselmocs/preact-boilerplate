import path from 'path';
import webpack from 'webpack';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
// import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';


const ENV = process.env.NODE_ENV || 'development';

module.exports = {
    context: path.resolve(__dirname, "src"),

    entry: {
        app: './index.js'
    },

    output: {
        path: path.resolve(__dirname, "build"),
        publicPath: '/',
        filename: 'assets/js/[name].[hash:8].js'
    },

    resolve: {
        alias: {
            styles: path.resolve(__dirname, "src/styles")
        }
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test:/\.(s*)css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: ENV !== 'production',
                                importLoaders: 1,
                                minimize: ENV === 'production'
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: ENV !== 'production'
                            }
                        }
                    ]
                })
            }
        ]
    },

    plugins: [
        // new BundleAnalyzerPlugin(),
        new CleanWebpackPlugin(['build']),
        new ExtractTextPlugin({
            filename: 'assets/css/[name].[hash:8].css',
            allChunks: true,
            disable: ENV !== 'production'
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(ENV)
        }),
        new HtmlWebpackPlugin({
            template: './index.html',
            title: 'Preact Boilerplate',
            minify: {
                collapseWhitespace: true
            }
        }),
        new CopyWebpackPlugin([
            { from: './manifest.json', to: './' },
            { from: './favicon.ico', to: './' },
            { from: './assets', to: './assets' }
        ])
    ],

    optimization: {
        minimizer: (
            ENV === 'production' ? [
                new UglifyJsPlugin({
                    uglifyOptions: {
                        ecma: 8,
                        output: {
                            comments: false
                        },
                        // https://github.com/mishoo/UglifyJS2/tree/harmony#compress-options
                        compress: {
                            drop_console: true,
                            hoist_funs: true,
                            keep_fargs: false,
                            pure_getters: true,

                            unsafe: true,
                            unsafe_arrows: true,
                            unsafe_comps: true,
                            unsafe_Function: true,
                            unsafe_math: true,
                            unsafe_methods: true,
                            unsafe_proto: true,
                            unsafe_regexp: true,
                            unsafe_undefined: true,
                            unused: true
                        }
                    }
                })
            ] : []
        )
    },

    stats: true,

    devtool: ENV === 'production' ? 'hidden-source-map' : 'cheap-module-eval-source-map',

    devServer: {
        inline: true,
        host: 'localhost',
        port: process.env.PORT || 8080,
        publicPath: '/',
        contentBase: './src',
        historyApiFallback: true,
        open: false
    }
};
